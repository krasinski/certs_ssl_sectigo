Role Name
=========

Génération de Certificat SSL, valide 2 ans.

Requirements
------------

Nécessite la librairie python2-cryptography
Si manquant installation du paquet dans le role create_csr.yml

Role Variables
--------------

Dans le répertoire roles/certs/defaults, le fichier main.yml définit les variables pour generer le csr et faire ensuite la requete à sectigo.
Renseigner les champs : _csr_gen_info, cert_mail et login, password, org_id de l'api sectigo.

Utilisation des playbooks :

- create_cert.yml : Génération d'un csr et de la clé privée, sousmission à sectigo, récupération du crt, copie dans /etc/pki/tls/certs et private avec backup si clé/certificat déjà présent.
Le csr est stocké dans /etc/pki/tls/certs.

parametres demandées :

cert_cn: CN du certificat (FQDN)

cert_san (non obligatoire): Subject Alternative Name du certificat liste séparé par des ',' exemple : (ALIAS1,ALIAS2...) avec ALIASx sous forme de FQDN

mail du créateur : email du demandeur

- create_cert_csr.yml : Comme create_cert.yml mais demande un certificat à partir d'un fichier CSR existant fourni.
parametres demandées :
csr_file: le chemin du fichier CSR


Dependencies
------------


Example Playbook
----------------


License
-------

BSD

Author Information
------------------

Nicolas Krasinski
